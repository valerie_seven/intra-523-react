import React, { Component } from 'react';
import './App.css';

export default class Message extends Component {
	
    render() {
    return (
      <div>
          <h1>{this.props.message}</h1>
      </div>
    );
  }
}
