import React, { Component } from 'react';

export default class TrouveNom extends Component {
	
	constructor(props) {
	    super(props);
	    this.state = {
	    		nom: ''
	    };

	    this.handleChange = this.handleChange.bind(this);
	    this.trouveNom = this.trouveNom.bind(this);
	 }
	
	handleChange(event) {
		  const target = event.target;
		  const value = target.value;
		  const name = target.name;

		  this.setState({
			  [name]: value
		  });
	 }
	
	trouveNom(event) {
		alert('nom: ' + this.state.nom);
		event.preventDefault();
	}
	
	render() {
		return (
			<form onSubmit={this.trouveNom} >
				<input type="text" name="nom" value={this.state.nom} onChange={this.handleChange} placeholder="TrouveNom"/>
				<button type="submit">Trouve</button>
			</form>
		);
	}

}
